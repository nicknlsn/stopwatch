//
//  ViewController.swift
//  Stopwatch
//
//  Created by Nick Nelson on 4/15/15.
//  Copyright (c) 2015 Nick Nelson. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var timer = NSTimer()
    var timeTracker = 0
    var startPoint = "00:00.00"

    // the object that gives us access to the display label
    @IBOutlet weak var timeDisplay: UILabel!
    @IBOutlet weak var startButton: UIButton!
    
    // called when stop button is pressed
    @IBAction func stop(sender: AnyObject) {
        timer.invalidate()
        startButton.enabled = true
    }

    // called when start button is pressed
    @IBAction func start(sender: AnyObject) {
        if startButton.titleLabel?.text == "Start" {
            // start the timer and change the start button to say stop
            timer = NSTimer.scheduledTimerWithTimeInterval(0.01, target: self, selector: Selector("updateTime"), userInfo: nil, repeats: true)
            startButton.setTitle("Stop", forState: UIControlState.Normal)
        } else if startButton.titleLabel?.text == "Stop" {
            // stop the timer and change the stop button to say start
            timer.invalidate()
            startButton.setTitle("Start", forState: UIControlState.Normal)
        }
        
        // start the timer
        //timer = NSTimer.scheduledTimerWithTimeInterval(0.01, target: self, selector: Selector("updateTime"), userInfo: nil, repeats: true)
        //startButton.enabled = false
    }

    // called when reset button is pressed
    @IBAction func reset(sender: AnyObject) {
        timer.invalidate()
        timeDisplay.text = startPoint
        timeTracker = 0
        startButton.setTitle("Start", forState: UIControlState.Normal)
    }

    // keeps track of time and displays the time elapsed in a human readable format
    func updateTime() {
        // keep track of time
        timeTracker++
        
        // get time breakdown in minutes, seconds, and miliseconds, and put them into string constants
        var minutes = String(timeTracker / 100 / 60 % 60)
        var seconds = String(timeTracker / 100 % 60)
        var miliSeconds = String(timeTracker % 100)
        
        // make sure we always have two digits in each number
        if minutes.characters.count == 1 {
            minutes = "0" + minutes
        }
        if seconds.characters.count == 1 {
            seconds = "0" + seconds
        }
        if miliSeconds.characters.count == 1 {
            miliSeconds = "0" + miliSeconds
        }
        
        // put the time on the screen
        timeDisplay.text = minutes + ":" + seconds + "." + miliSeconds
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        startButton.setTitle("Start", forState: UIControlState.Normal)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        print("did enter background")
    }
}

